//////////////////////////////////////////////////////////////////
//
//The class BaseLog is the base class of the Logentry class.
//It provides the timestamp- and "evaluation of all logs"
//functionality. The Logentry class will be implemented as
//template and add the payload functionality
//
//////////////////////////////////////////////////////////////////

#include <iostream>
#include <algorithm>

#include "baselog.h"

//Container with all Logentries for final evaluation.
//Must be evaluated and cleared after each test case.
vector<BaseLog*> BaseLog::smvlAllLogs;

//The next time stamp is incremented for each instance
//of Logentry. 0 is a forbidden value due to overflow protection.
int BaseLog::smiNextTimeStamp = 1;

//The Ctor registers the instance and assigns the next time stamp.
BaseLog::BaseLog(void)
{
  //Registration:
  mbIsValidated = false;
  smvlAllLogs.push_back(this);

  //Time stamp:
  if(smiNextTimeStamp == 0)
  {
    cout << "ERROR: Time stamp overflow: Too much logs instantiated!";
    //TODO: Make the test case fail.
  }

  miTimeStamp = smiNextTimeStamp++;
}

//Getter for the time stamp.
int BaseLog::GetTimeStamp(void)
{
  return miTimeStamp;
}

//Checks, if all logs are evaluated. The Logentry class
//will provide the functionality to evaluate a log.
bool BaseLog::AllLogsEvaluated(void)
{
  bool retVal(true);

  //Check, if there is any log that has not been validated:
  for(const auto log : smvlAllLogs)
  {
    if(log == nullptr || !log->mbIsValidated)
    {
      //if log==nullptr, the log was irregular destructed
      retVal = static_cast<bool>(false);
      break;
    }
  }

  //Test case finished. Prepare for the next one!
  smvlAllLogs.clear();

  return retVal;
}

//If logs are stored in container classes, that use dynamic reallocation,
//move semantics must be used, to change the address of the log in the global
//log list to the new one.
BaseLog::BaseLog(BaseLog &&rhs) noexcept
{
  //Replace the address of the old instance with the new one:
  auto itOnRhs = find(smvlAllLogs.begin(), smvlAllLogs.end(), &rhs);

  if(itOnRhs != smvlAllLogs.end())
  {
    *itOnRhs = this;
  }
  else
  {
    cout << "ERROR: Reallocating Logentry: Couldn�t find address within the global log list. Something went wrong." << endl;
  }

  miTimeStamp = rhs.GetTimeStamp();
}

//If a log is unintentionally deleted before the evaluation of all logs via
//AllLogEvaluated() took place, the reference to the deleted log must be set
//to null, to make the test case fail.
BaseLog::~BaseLog(void) noexcept
{
  //Invalidate stored pointer to this instance. If this destructor was called
  //during the reallocation process of a container class, the move constructor
  //has already replaced the address in the global log list and the following
  //code will do nothing, which is fine:
  auto itOnMe = find(smvlAllLogs.begin(), smvlAllLogs.end(), this);

  if(itOnMe != smvlAllLogs.end())
  {
    *itOnMe = nullptr;
  }
}


