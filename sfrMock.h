//////////////////////////////////////////////////////////////////
//
//  The class SFR_Mock is used to mock special function registers.
//  Whenever it is written, a log is created.
//
//////////////////////////////////////////////////////////////////

#pragma once

#include <vector>
#include <iostream>
#include "logentry.h"

class SFR_Mock
{
private:
  //  All logs, stored by a particular mock.
  vector<Logentry<unsigned char>> vLogs;

  //  Name of the mock. Used for more readable
  //  warnings and error messages.
  string name;

public:

  SFR_Mock(string theName);

  //  Write operation to SFR creates an Logentry instance
  //  and stores it.
  SFR_Mock& operator=(const unsigned char &val);

  //  This search algorithm and others can be moved to a base class
  //  for other mock classes in the future.
  Logentry<unsigned char>& GetLog(size_t index);

  //  Deletes all stored logs.
  //  Should be called before each test case to prepare the mock.
  void Clear(void);

};
