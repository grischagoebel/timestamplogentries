//////////////////////////////////////////////////////////////////
//
//SFR mock implementation.
//
//////////////////////////////////////////////////////////////////

#include "sfrMock.h"

SFR_Mock::SFR_Mock(string theName) : name(theName){};

//Write operation to SFR creates an Logentry instance
//and stores it.
SFR_Mock& SFR_Mock::operator=(const unsigned char &val)
{
    vLogs.emplace_back(val);
    return *this;
};

//This search algorithm and others can be moved to a base class
//for other mock classes in the future.
Logentry<unsigned char>& SFR_Mock::GetLog(size_t index)
{
    return vLogs[index];
};

//Deletes all stored logs.
//Should be called before each test case to prepare the mock.
void SFR_Mock::Clear(void)
{
    vLogs.clear();
}
