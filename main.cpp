//////////////////////////////////////////////////////////////////
//
//  The main method executes four example test cases. The first
//  three are demonstrating the basic behavior of the Logentry
//  class and the fourth one tests an imaginary function that
//  uses two UART related special function registers.
//  Because the test organization with test case macros is out of
//  the scope of this project, the test cases will be implemented
//  within simple functions.
//
//////////////////////////////////////////////////////////////////

#include <iostream>
#include "logentry.h"
#include "evaluator.h"
#include "targetSfrDefinitions.h"
#include "uartToTest.h"

using namespace std;

//  This should be included in the test execution framework:
void PerformFinalCheck(void)
{
  //  Check for unspecified behaviour:
  cout << "All logs evaluated? " << (BaseLog::AllLogsEvaluated() ? "True" : "ERRRO: Not all logs evaluated!") << endl;

  //  Prepare for next test case by clearing all Logentries:
  UARTAR.Clear();
  UARTDR.Clear();
}

//  Example for evaluation of a Logentry:
//  Create a Logentry and the evaluate the Loogentry
void TestCase_Log_1(void)
{
  Logentry<int> log(4);
  TEST_ASSERT(log.Equals(4));    //OK
  TEST_ASSERT(!log.Equals(5));   //OK
  TEST_ASSERT(log.Equals(6));    //FAIL

  PerformFinalCheck();           //OK
}

//  Example for Sequence evaluation:
void TestCase_Log_2(void)
{
  Logentry<int> logFirst(10);
  Logentry<int> logSecond(11);

  //  Valid sequence:
  START_SEQUENCE();

  //  This sequence assert evaluates the value
  //  of the payload and the timestamps of the
  //  Logentrys:
  TEST_SEQ_LOG_EQUALS(logFirst, 10);  //  Payload OK
  TEST_SEQ_LOG_EQUALS(logSecond, 11); //  Payload OK and logFirst was instantiated before logSecond

  //  New sequence:
  //  Invalid sequence:
  START_SEQUENCE();

  TEST_SEQ_LOG_EQUALS(logSecond, 11); //  Payload OK
  TEST_SEQ_LOG_EQUALS(logFirst, 7);   //  FAIL: logFirst was instantiated before logSecond
                                      //  and invalid payload.

  PerformFinalCheck();                //  OK
}

//  Example for a not evaluated Logentry:
void TestCase_Log_3(void)
{
  //  Test the final check:
  Logentry<string> log3("I will not be evaluated!");

  PerformFinalCheck();  //  FAIL
}

//  Test of the WriteUART() function with one simple value.
//  The SFRs UARTAR and UARTDR are replaced by mocks that rely on
//  the Logentry class.
void TestCase_UART(void)
{
  //  Arrange
  unsigned char theValue(0x07);

  //  Act
  WriteUART(theValue);

  //  Assert
  START_SEQUENCE();

  TEST_SEQ_LOG_EQUALS(UARTAR.GetLog(0), 0x0A);
  TEST_SEQ_LOG_EQUALS(UARTDR.GetLog(0), theValue);
  TEST_SEQ_LOG_EQUALS(UARTDR.GetLog(1), '\n');
  TEST_SEQ_LOG_EQUALS(UARTAR.GetLog(1), 0x00);

  PerformFinalCheck();
}

int main()
{

  cout << "\n\nTest 1:\n" << endl;
  TestCase_Log_1();

  cout << "\n\nTest 2:\n" << endl;
  TestCase_Log_2();

  cout << "\n\nTest 3:\n" << endl;
  TestCase_Log_3();


  cout << "\n\nOne more \"real life\" example:\n" << endl;
  TestCase_UART();

  return 0;
}
