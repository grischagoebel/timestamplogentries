#pragma once

#include "targetSfrDefinitions.h"

//  The function WriteUART() must write the given value, followed by '\n',
//  by implementing the following requirements in the specified order:
//
//  The function WriteUART() must select the UART write register by setting
//  the UARTAR register to 0x0A.
//
//  The function WriteUART must write the given value to the UARTDR register.
//
//  The function WriteUART must write the value '\n' to the UARTDR register.
//
//  The function WriteUART must write the value 0x00 to the UARTAR register.

void WriteUART(unsigned char byteValue)
{
  UARTAR = static_cast<unsigned char>(0x0A);

  //  UARTAR = static_cast<unsigned char>(0x00); //Uncomment this line and comment the last UARTAR access to test the sequence evaluation.

  UARTDR = byteValue;
  UARTDR = static_cast<unsigned char>('\n');
  UARTAR = static_cast<unsigned char>(0x00);

  //  Unspecified behavior:
  //  Uncomment to make the test case automatically fail!
  //  UARTDR = static_cast<unsigned char>(0xFF);
}
