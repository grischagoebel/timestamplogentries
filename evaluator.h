//////////////////////////////////////////////////////////////////
//
//The class Evaluator implements the functionality of the test
//macros. It can communicate with other frameworks that organize
//test execution and decide, if a test case failed or not, by
//making the current test case fail, if necessary.
//
//////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "baselog.h"
#include "logentry.h"

//Evaluates a condition
#define TEST_ASSERT(__condition)                                            \
  Evaluator::Instance().TestAssert(__condition, #__condition, __LINE__);    \

//Starts the evaluation of a sequence of events, represented by logs.
#define START_SEQUENCE()                 \
  Evaluator::Instance().StartSequence()  \

//Evaluates a condition and a sequence based on an instance of a Logentry.
#define TEST_SEQ_LOG_EQUALS(__log, __payload)                                                                \
  Evaluator::Instance().TestSequence(__log.GetTimeStamp(), __log.Equals(__payload), #__log, #__payload, __LINE__) \

class Evaluator
{
private:
  //Holds the last time stamp, when evaluating a sequence.
  int miLastTimeStamp;

  //Singleton pattern: Prevent instantiation through private access specifier.
  Evaluator(void);

public:

  //Singleton pattern: Provide the singleton instance.
  static Evaluator& Instance(void);

  //Evaluates the condition. If the condition is false,
  //the string representation of the condition is printed together
  //with the line number.
  void TestAssert(bool bCondition, string strCondition, int iLine);

  //Starts the evaluation of a sequence of events, represented by logs.
  void StartSequence(void);

  //Similar to TestAssert(), but also checks, that iTimeStamp is greater
  //then miLastTimeStamp and updates miLastTimeStamp.
  //Used to evaluate Sequences.
  void TestSequence(int iTimeStamp, bool bCondition, string strLog, string strPayload, int iLine);
};
