//////////////////////////////////////////////////////////////////
//
//  The class Logentry is used, to log events in a mock class during
//  test execution.
//  Each Logentry stores data as payload that must be compared to an
//  expected value in order to evaluate the Logentry. If there is any
//  Logentry at the end of the assert phase of the test case, that
//  has not been evaluated, the test execution framework must make
//  the test case fail. This mechanism assures, that unspecified
//  behavior implicitly fails a test case without further effort.
//
//////////////////////////////////////////////////////////////////

#pragma once

#include "baselog.h"


template<typename T>
class Logentry : public BaseLog
{
private:
  //  The stored value, that must be evaluated by comparison
  //  (only == is implemented), to pass the test case that
  //  instantiated the Logentry.
  T payload;

public:

  Logentry(void) {};
  //  Move constructor used for container classes, that perform dynamic
  //  reallocation. Functionality is implemented in BaseLog.
  Logentry(Logentry &&rhs) = default;

  //  No other constructors, no assignement operators.
  //  Logentries must only be instantiated and validated.
  Logentry(T payload) : payload(payload){}

  //  Getter for the payload without evaluating the log:
  T& GetPayload(void)
  {
    return payload;
  }

  //  Don�t use this operator:
  //  Logentry == T will cause a implicit cast T to Logentry
  //  using the constructor BaseLog() resulting in a registered
  //  log that should have been just a temporary without
  //  further usage.
  bool operator==(const Logentry& other) = delete;

  //  Use this operator instead:
  bool Equals(const T& expectedPayload)
  {
    mbIsValidated = true;
    return payload == expectedPayload;
  }
};
