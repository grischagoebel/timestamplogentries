//////////////////////////////////////////////////////////////////
//
//  The SFR_Mock class implements a mock, whose instances can be
//  used to replace SFR definitions.
//  The SFR_Mock class logs write accesses to the replaced SFR, by
//  storing the written value for later evaluation in a Logentry
//  instance.
//
//////////////////////////////////////////////////////////////////

#pragma once

#include "sfrMock.h"
#include <vector>
#include <iostream>

class SFR_Mock;

extern SFR_Mock UARTAR;
extern SFR_Mock UARTDR;
