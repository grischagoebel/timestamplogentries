//////////////////////////////////////////////////////////////////
//
//The class Evaluator implements the functionality of the test
//macros. It can communicate with other frameworks that organize
//test execution and decide, if a test case failed or not, by
//making the current test case fail, if necessary.
//
//////////////////////////////////////////////////////////////////

#include <iostream>

#include "evaluator.h"

//Constructor sets the last time stamp to zero.
//The last time stamp is used when sequences are evaluated.
Evaluator::Evaluator(void)
{
  miLastTimeStamp = 0;
}

//Singleton pattern.
Evaluator& Evaluator::Instance(void)
{
  static Evaluator theInstance;
  return theInstance;
}

//Implements the functionality of the TEST_ASSERT macro.
void Evaluator::TestAssert(bool bCondition, string strCondition, int iLine)
{
  if(!bCondition)
  {
    //Make current test case fail
    cout << "ERROR: " << strCondition << " failed in line " << iLine << "!" << endl;
  }
}

//Implements the functionality of the START_SEQUENCE macro.
void Evaluator::StartSequence(void)
{
  miLastTimeStamp = 0;
}

//Implements the functionality of the TEST_SEQ_LOG_EQUALS macro.
//The condition will be evaluated and the given time stamp must be greater
//than the last one.
void Evaluator::TestSequence(int iTimeStamp, bool bCondition, string strLog, string strPayload, int iLine)
{
  if(iTimeStamp <= miLastTimeStamp)
  {
    //Make current test case fail
    cout << "ERROR: " << strLog << " is not successor of previous log in line " << iLine << "!" << endl;
  }

  miLastTimeStamp = iTimeStamp;

  if(!bCondition)
  {
    //Make current test case fail
    cout << "ERROR: Payload of " << strLog << " is not equal to " << strPayload << " in line " << iLine << "!" << endl;
  }

}
