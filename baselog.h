//////////////////////////////////////////////////////////////////
//
//  The class BaseLog stores all logs, checks if they are
//  evaluated and manages the global time stamp.
//
//////////////////////////////////////////////////////////////////

#pragma once

#include <vector>

using namespace std;

class BaseLog
{
private:
  //  Pointer to all logs created in current test case.
  //  The destructor assures, that no log can be destructed
  //  before the evaluation of validation via
  //  AllLogsAreValidated() took place.
  static vector<BaseLog*> smvlAllLogs;

  //  Each log gets its own time stamp based
  //  on this global time.
  //  MUST be overflow protected -> init with 1!
  static int smiNextTimeStamp;

  //  Time stamp of the instance.
  int miTimeStamp;

protected:

  //  Is true, if the log was already evaluated.
  bool mbIsValidated;

  //  Constructor. This class must not be instantiated.
  BaseLog(void);

  //  Move constructor for container classes, that perform
  //  dynamic reallocation.
  //  Without move semantics, the destructor would make the
  //  test case fail during final evaluation with
  //  AllLogsEvaluated().
  //  noexcept for the move constructor and the destructor is
  //  necessary for the stl container to use move semantics
  //  during reallocation.
  BaseLog(BaseLog &&rhs) noexcept;

public:

  //  Getter for the time stamp.
  int GetTimeStamp(void);

  //  Checks, if all logs where evaluated and clears the
  //  list of pointers mvlAllLogs.
  //  MUST be called before any instance of a log was
  //  deleted to return true.
  static bool AllLogsEvaluated(void);

  //  The destructor assures, that the destruction sets the
  //  corresponding pointer in the global log list to null.
  //  The final evaluation with AllLogsEvaluated can only be
  //  successful, if no log has been deleted.
  ~BaseLog(void) noexcept;
};
